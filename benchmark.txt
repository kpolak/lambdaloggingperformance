# JMH 1.18 (released 25 days ago)
# VM version: JDK 1.8.0_121, VM 25.121-b13
# VM invoker: /Library/Java/JavaVirtualMachines/jdk1.8.0_121.jdk/Contents/Home/jre/bin/java
# VM options: <none>
# Warmup: 5 iterations, 500 ms each
# Measurement: 5 iterations, 500 ms each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: LambdaLoggingBenchmark.lambdaLogging

# Run progress: 0.00% complete, ETA 00:00:30
# Fork: 1 of 3
# Warmup Iteration   1: 0.679 ns/op
# Warmup Iteration   2: 0.706 ns/op
# Warmup Iteration   3: 0.650 ns/op
# Warmup Iteration   4: 0.678 ns/op
# Warmup Iteration   5: 0.683 ns/op
Iteration   1: 0.688 ns/op
Iteration   2: 0.670 ns/op
Iteration   3: 0.679 ns/op
Iteration   4: 0.679 ns/op
Iteration   5: 0.678 ns/op

# Run progress: 16.67% complete, ETA 00:00:26
# Fork: 2 of 3
# Warmup Iteration   1: 0.672 ns/op
# Warmup Iteration   2: 0.673 ns/op
# Warmup Iteration   3: 0.661 ns/op
# Warmup Iteration   4: 0.679 ns/op
# Warmup Iteration   5: 0.667 ns/op
Iteration   1: 0.656 ns/op
Iteration   2: 0.651 ns/op
Iteration   3: 0.725 ns/op
Iteration   4: 0.694 ns/op
Iteration   5: 0.695 ns/op

# Run progress: 33.33% complete, ETA 00:00:21
# Fork: 3 of 3
# Warmup Iteration   1: 0.675 ns/op
# Warmup Iteration   2: 0.684 ns/op
# Warmup Iteration   3: 0.662 ns/op
# Warmup Iteration   4: 0.651 ns/op
# Warmup Iteration   5: 0.706 ns/op
Iteration   1: 0.793 ns/op
Iteration   2: 0.745 ns/op
Iteration   3: 0.644 ns/op
Iteration   4: 0.647 ns/op
Iteration   5: 0.651 ns/op

Result "LambdaLoggingBenchmark.lambdaLogging":
  0.686 ±(99.9%) 0.044 ns/op [Average]
  (min, avg, max) = (0.644, 0.686, 0.793), stdev = 0.041
  CI (99.9%): [0.642, 0.730] (assumes normal distribution)

Secondary result "LambdaLoggingBenchmark.lambdaLogging:·sun.gc.generation.0.space.0.used":
  268435.200 ±(99.9%) 594091.609 ? [Average]
  (min, avg, max) = (≈ 0, 268435.200, 1342176.000), stdev = 555713.659
  CI (99.9%): [≈ 0, 862526.809] (assumes normal distribution)



# JMH 1.18 (released 25 days ago)
# VM version: JDK 1.8.0_121, VM 25.121-b13
# VM invoker: /Library/Java/JavaVirtualMachines/jdk1.8.0_121.jdk/Contents/Home/jre/bin/java
# VM options: <none>
# Warmup: 5 iterations, 500 ms each
# Measurement: 5 iterations, 500 ms each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: LambdaLoggingBenchmark.oldSchooldLogging

# Run progress: 50.00% complete, ETA 00:00:16
# Fork: 1 of 3
# Warmup Iteration   1: 0.508 ns/op
# Warmup Iteration   2: 0.521 ns/op
# Warmup Iteration   3: 0.501 ns/op
# Warmup Iteration   4: 0.489 ns/op
# Warmup Iteration   5: 0.492 ns/op
Iteration   1: 0.529 ns/op
Iteration   2: 0.523 ns/op
Iteration   3: 0.499 ns/op
Iteration   4: 0.511 ns/op
Iteration   5: 0.523 ns/op

# Run progress: 66.67% complete, ETA 00:00:10
# Fork: 2 of 3
# Warmup Iteration   1: 0.546 ns/op
# Warmup Iteration   2: 0.534 ns/op
# Warmup Iteration   3: 0.517 ns/op
# Warmup Iteration   4: 0.516 ns/op
# Warmup Iteration   5: 0.505 ns/op
Iteration   1: 0.508 ns/op
Iteration   2: 0.499 ns/op
Iteration   3: 0.504 ns/op
Iteration   4: 0.485 ns/op
Iteration   5: 0.500 ns/op

# Run progress: 83.33% complete, ETA 00:00:05
# Fork: 3 of 3
# Warmup Iteration   1: 0.537 ns/op
# Warmup Iteration   2: 0.506 ns/op
# Warmup Iteration   3: 0.516 ns/op
# Warmup Iteration   4: 0.487 ns/op
# Warmup Iteration   5: 0.501 ns/op
Iteration   1: 0.518 ns/op
Iteration   2: 0.505 ns/op
Iteration   3: 0.512 ns/op
Iteration   4: 0.509 ns/op
Iteration   5: 0.499 ns/op



Result "LambdaLoggingBenchmark.oldSchooldLogging":
  0.508 ±(99.9%) 0.012 ns/op [Average]
  (min, avg, max) = (0.485, 0.508, 0.529), stdev = 0.012
  CI (99.9%): [0.496, 0.521] (assumes normal distribution)

Secondary result "LambdaLoggingBenchmark.oldSchooldLogging:·sun.gc.generation.0.space.0.used":
  268435.200 ±(99.9%) 594091.609 ? [Average]
  (min, avg, max) = (≈ 0, 268435.200, 1342176.000), stdev = 555713.659
  CI (99.9%): [≈ 0, 862526.809] (assumes normal distribution)

# Run complete. Total time: 00:00:32


Benchmark                                                                                           Mode  Cnt       Score        Error  Units
LambdaLoggingBenchmark.lambdaLogging                                                                avgt   15       0.686 ±      0.044  ns/op
LambdaLoggingBenchmark.lambdaLogging:·sun.gc.generation.0.space.0.used                              avgt   15  268435.200 ± 594091.609      ?

LambdaLoggingBenchmark.oldSchooldLogging                                                            avgt   15       0.508 ±      0.012  ns/op
LambdaLoggingBenchmark.oldSchooldLogging:·sun.gc.generation.0.space.0.used                          avgt   15  268435.200 ± 594091.609      ?
