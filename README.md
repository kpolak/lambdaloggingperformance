## Lambda Performance Test

This is a simple comparison of oldSchool vs Lambda logging approaches.

## Running

1. Build the project: mvn clean install
2. Execute java -jar target/benchmarks.jar -prof hs_gc

## External docs

http://openjdk.java.net/projects/code-tools/jmh/

## License

Everybody can reuse the code just after informing me.
