import kpolak.logging.mockups.LambdaLoggerMockup;
import kpolak.logging.mockups.OldSchoolLoggerMockup;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;


/**
 * Created by kpolak on 07/04/2017.
 */
@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 5, time = 500, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 500, timeUnit = TimeUnit.MILLISECONDS)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Fork(3)
@State(Scope.Benchmark)
public class LambdaLoggingBenchmark {

    private final static OldSchoolLoggerMockup oldSchoolLogger = new OldSchoolLoggerMockup();
    private final static LambdaLoggerMockup lambdaLogger = new LambdaLoggerMockup();

    @Benchmark
    public void oldSchooldLogging() {
        String s1 = "text 1";
        if (oldSchoolLogger.isDebugEnabled()) {
            oldSchoolLogger.debug("Text1" + s1);
        }

        if (oldSchoolLogger.isDebugEnabled()) {
            oldSchoolLogger.debug("Text2" + "Text3" + "Text4");
        }

        if (oldSchoolLogger.isDebugEnabled()) {
            oldSchoolLogger.debug("Text5" + s1);
        }
    }


    @Benchmark
    public void lambdaLogging() {
        final String methodName = "lambdaLogging";
        String s1 = "text 1";
        lambdaLogger.logDebug(methodName, () -> "Text1" + s1);

        lambdaLogger.logDebug(methodName, () -> "Text2" + "Text3" + "Text4");

        lambdaLogger.logDebug(methodName, () -> "Text5" + s1);
    }


}
