package kpolak.logging.mockups;

import java.util.function.Supplier;

/**
 * Created by kpolak on 07/04/2017.
 */
public class LambdaLoggerMockup {

    static String SOURCE_CLASS_NAME = "SOUECE_CLASS";

    private boolean debugEnabled = false;

    public void logDebug(String sourceMethod, Supplier<String> msgSupplier) {
        if (isDebugEnabled()) {
            debug(String.format("%s.%s - %s", SOURCE_CLASS_NAME, sourceMethod, msgSupplier.get()));
        }
    }

    public boolean isDebugEnabled() {
        return debugEnabled;
    }

    public void debug(String message) {
        System.out.println(message);
    }

}
