package kpolak.logging.mockups;

/**
 * Created by kpolak on 07/04/2017.
 */
public class OldSchoolLoggerMockup {

    private boolean debugEnabled = false;


    public boolean isDebugEnabled() {
        return debugEnabled;
    }

    public void debug(String message) {
        System.out.println(message);
    }
}
